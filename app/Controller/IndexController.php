<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class IndexController extends AppController {

	var $helpers = array('Number');

	public function index() {
		$this->loadModel('Product');
		//debug($this->Product->find('all')); die;
		$this->set('products', $this->Product->find('all'));
	}
}